/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Template_HeaderExample.h
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Example for using the .h file template. The brief should be kept as short as possible (maximum of
 *                three lines, with respect to the given format and without passing over the 120 characters per line),
 *                and only general information should be provided in this section.
 *
 *    Example detailed description of the template header. This paragraph has the purpose of providing details that
 *    cannot be integrated in the brief description, since it would take too much space.
 *    This section is not mandatory and shall be used only when a brief description of the file is not enough. In case
 *    this section is not needed then the line containing the end of comment '*'/ shall be the only line between the
 *    last line of the brief and the border, like in the Template_HeaderEmpty.h file.
 *    The limit of 120 characters per line applies to all comments and it is also included in the formatter.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

#ifndef TEMPLATE_HEADER_EXAMPLE_H
#define TEMPLATE_HEADER_EXAMPLE_H

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Std_Types.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of an macro defining the lengths of the exported arrays. */
#define TEMPLATE_GLOBAL_MACRO_LENGTH (1U)

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of an exported simple data type. */
typedef uint8 Template_GlobalSimpleType;

/** \brief  Example of an exported function type. */
typedef void (Template_GlobalFunctionType)(void);

/** \brief  Example of an exported structure data type. */
typedef struct
{
   /** \brief  First member of the example structure type. */
   uint8 uc_UnsignedChar;

   /** \brief  Second member of the example structure type. */
   Template_GlobalSimpleType t_CustomSimple;

   /** \brief  Third member of the example structure type. */
   Template_GlobalSimpleType * pt_PointerCustomSimple;

   /** \brief  Fourth member of the example structure type. */
   Template_GlobalFunctionType * pf_PointerFunction;
} Template_GlobalStructureType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern const uint8 Template_gkuc_GlobalConstantUnsignedChar;
extern const uint8 * Template_gkpuc_GlobalConstantPointerUnsignedChar;
extern const uint8 Template_gkauc_GlobalConstantArrayUnsignedChar[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern const sint8 Template_gksc_GlobalConstantSignedChar;
extern const sint8 * Template_gkpsc_GlobalConstantPointerSignedChar;
extern const sint8 Template_gkasc_GlobalConstantArraySignedChar[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern const uint16 Template_gkus_GlobalConstantUnsignedShort;
extern const uint16 * Template_gkpus_GlobalConstantPointerUnsignedShort;
extern const uint16 Template_gkaus_GlobalConstantArrayUnsignedShort[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern const sint16 Template_gkss_GlobalConstantSignedShort;
extern const sint16 * Template_gkpss_GlobalConstantPointerSignedShort;
extern const sint16 Template_gkass_GlobalConstantArraySignedShort[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern const uint32 Template_gkul_GlobalConstantUnsignedLong;
extern const uint32 * Template_gkpul_GlobalConstantPointerUnsignedLong;
extern const uint32 Template_gkaul_GlobalConstantArrayUnsignedLong[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern const sint32 Template_gksl_GlobalConstantSignedLong;
extern const sint32 * Template_gkpsl_GlobalConstantPointerSignedLong;
extern const sint32 Template_gkasl_GlobalConstantArraySignedLong[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern const Template_GlobalStructureType Template_gkt_GlobalConstantCustomType;
extern const Template_GlobalStructureType * Template_gkpt_GlobalConstantPointerCustomType;
extern const Template_GlobalStructureType Template_gkat_GlobalConstantArrayCustomType[TEMPLATE_GLOBAL_MACRO_LENGTH];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern uint8 Template_guc_GlobalVariableUnsignedChar;
extern uint8 * Template_gpuc_GlobalVariablePointerUnsignedChar;
extern uint8 Template_gauc_GlobalVariableArrayUnsignedChar[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern sint8 Template_gsc_GlobalVariableSignedChar;
extern sint8 * Template_gpsc_GlobalVariablePointerSignedChar;
extern sint8 Template_gasc_GlobalVariableArraySignedChar[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern uint16 Template_gus_GlobalVariableUnsignedShort;
extern uint16 * Template_gpus_GlobalVariablePointerUnsignedShort;
extern uint16 Template_gaus_GlobalVariableArrayUnsignedShort[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern sint16 Template_gss_GlobalVariableSignedShort;
extern sint16 * Template_gpss_GlobalVariablePointerSignedShort;
extern sint16 Template_gass_GlobalVariableArraySignedShort[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern uint32 Template_gul_GlobalVariableUnsignedLong;
extern uint32 * Template_gpul_GlobalVariablePointerUnsignedLong;
extern uint32 Template_gaul_GlobalVariableArrayUnsignedLong[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern sint32 Template_gsl_GlobalVariableSignedLong;
extern sint32 * Template_gpsl_GlobalVariablePointerSignedLong;
extern sint32 Template_gasl_GlobalVariableArraySignedLong[TEMPLATE_GLOBAL_MACRO_LENGTH];
extern Template_GlobalStructureType Template_gt_GlobalVariableCustomType;
extern Template_GlobalStructureType * Template_gpt_GlobalVariablePointerCustomType;
extern Template_GlobalStructureType Template_gat_GlobalVariableArrayCustomType[TEMPLATE_GLOBAL_MACRO_LENGTH];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

extern void Template_gv_GlobalFunctionNoReturn(void);
extern uint8 Template_guc_GlobalFunctionUnsignedCharReturn(void);
extern uint8 * Template_gpuc_GlobalFunctionPointerUnsignedCharReturn(void);
extern sint8 Template_gsc_GlobalFunctionSignedCharReturn(void);
extern sint8 * Template_gpsc_GlobalFunctionPointerSignedCharReturn(void);
extern uint16 Template_gus_GlobalFunctionUnsignedShortReturn(void);
extern uint16 * Template_gpus_GlobalFunctionPointerUnsignedShortReturn(void);
extern sint16 Template_gss_GlobalFunctionSignedShortReturn(void);
extern sint16 * Template_gpss_GlobalFunctionPointerSignedShortReturn(void);
extern uint32 Template_gul_GlobalFunctionUnsignedLongReturn(void);
extern uint32 * Template_gpul_GlobalFunctionPointerUnsignedLongReturn(void);
extern sint32 Template_gsl_GlobalFunctionSignedLongReturn(void);
extern sint32 * Template_gpsl_GlobalFunctionPointerSignedLongReturn(void);
extern Template_GlobalStructureType Template_gt_GlobalFunctionCustomTypeReturn(void);
extern Template_GlobalStructureType * Template_gpt_GlobalFunctionPointerCustomTypeReturn(void);

/*-------------------------------------------------------------------------------------------------------------------*/
#endif /* TEMPLATE_HEADER_EXAMPLE_H */

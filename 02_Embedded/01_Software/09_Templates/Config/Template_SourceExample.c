/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Template_SourceExample.c
 *    \author     Nicolae-Bogdan Bacrau
 *    \brief      Example for using the .c file template. The brief should be kept as short as possible (maximum of
 *                three lines, with respect to the given format and without passing over the 120 characters per line),
 *                and only general information should be provided in this section.
 *
 *    Example detailed description of the template header. This paragraph has the purpose of providing details that
 *    cannot be integrated in the brief description, since it would take too much space.
 *    This section is not mandatory and shall be used only when a brief description of the file is not enough. In case
 *    this section is not needed then the line containing the end of comment '*'/ shall be the only line between the
 *    last line of the brief and the border, like in the Template_SourceEmpty.c file.
 *    The limit of 120 characters per line applies to all comments and it is also included in the formatter.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/

#include "Template_HeaderExample.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

#if (TEMPLATE_GLOBAL_MACRO_LENGTH != 0U)

#if (TEMPLATE_GLOBAL_MACRO_LENGTH == 6U)

/** \brief  Example of defining a local macro based on ifdefs. */
#define TEMPLATE_LOCAL_MACRO  (6U)

#elif (TEMPLATE_GLOBAL_MACRO_LENGTH == 4U)

/** \brief  Example of defining a local macro based on ifdefs. */
#define TEMPLATE_LOCAL_MACRO  (4U)

#elif (TEMPLATE_GLOBAL_MACRO_LENGTH == 2U)

/** \brief  Example of defining a local macro based on ifdefs. */
#define TEMPLATE_LOCAL_MACRO  (2U)

#else

/** \brief  Example of defining a local macro based on ifdefs. */
#define TEMPLATE_LOCAL_MACRO  (1U)

#endif /* (TEMPLATE_GLOBAL_MACRO_LENGTH == 6U) */

#endif /* (TEMPLATE_GLOBAL_MACRO_LENGTH != 0U) */

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of defining a local constant simple data type. */
typedef uint8 Template_LocalSimpleType;

/** \brief  Example of defining a local function type. */
typedef void (Template_LocalFunctionType)(void);

/** \brief  Example of defining a local structure data type. */
typedef struct
{
   /** \brief  First member of the example structure type. */
   uint8 uc_UnsignedChar;

   /** \brief  Second member of the example structure type. */
   Template_LocalSimpleType t_CustomSimple;

   /** \brief  Third member of the example structure type. */
   Template_LocalSimpleType * pt_PointerCustomSimple;

   /** \brief  Fourth member of the example structure type. */
   Template_LocalFunctionType * pf_PointerFunction;
} Template_LocalStructureType;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of declaring a local constant unsigned char. */
static const uint8 Template_kuc_LocalConstantUnsignedChar = 0U;

/** \brief  Example of declaring a local constant unsigned char pointer. */
static const uint8 * Template_kpuc_LocalConstantPointerUnsignedChar = NULL_PTR;

/** \brief  Example of declaring a local constant unsigned char array. */
static const uint8 Template_kauc_LocalConstantArrayUnsignedChar[TEMPLATE_LOCAL_MACRO] =
{ 0U };

/** \brief  Example of declaring a local constant signed char. */
static const sint8 Template_ksc_LocalConstantSignedChar = 0;

/** \brief  Example of declaring a local constant signed char pointer. */
static const sint8 * Template_kpsc_LocalConstantPointerSignedChar = NULL_PTR;

/** \brief  Example of declaring a local constant signed char array. */
static const sint8 Template_kasc_LocalConstantArraySignedChar[TEMPLATE_LOCAL_MACRO] =
{ 0 };

/** \brief  Example of declaring a local constant unsigned short. */
static const uint16 Template_kus_LocalConstantUnsignedShort = 0U;

/** \brief  Example of declaring a local constant unsigned short pointer. */
static const uint16 * Template_kpus_LocalConstantPointerUnsignedShort = NULL_PTR;

/** \brief  Example of declaring a local constant unsigned short array. */
static const uint16 Template_kaus_LocalConstantArrayUnsignedShort[TEMPLATE_LOCAL_MACRO] =
{ 0U };

/** \brief  Example of declaring a local constant signed short. */
static const sint16 Template_kss_LocalConstantSignedShort = 0;

/** \brief  Example of declaring a local constant signed short pointer. */
static const sint16 * Template_kpss_LocalConstantPointerSignedShort = NULL_PTR;

/** \brief  Example of declaring a local constant signed short array. */
static const sint16 Template_kass_LocalConstantArraySignedShort[TEMPLATE_LOCAL_MACRO] =
{ 0 };

/** \brief  Example of declaring a local constant unsigned long. */
static const uint32 Template_kul_LocalConstantUnsignedLong =
{ 0UL };

/** \brief  Example of declaring a local constant unsigned long pointer. */
static const uint32 * Template_kpul_LocalConstantPointerUnsignedLong = NULL_PTR;

/** \brief  Example of declaring a local constant unsigned long array. */
static const uint32 Template_kaul_LocalConstantArrayUnsignedLong[TEMPLATE_LOCAL_MACRO] =
{ 0UL };

/** \brief  Example of declaring a local constant signed long. */
static const sint32 Template_ksl_LocalConstantSignedLong =
{ 0L };

/** \brief  Example of declaring a local constant signed long pointer. */
static const sint32 * Template_kpsl_LocalConstantPointerSignedLong = NULL_PTR;

/** \brief  Example of declaring a local constant signed long array. */
static const sint32 Template_kasl_LocalConstantArraySignedLong[TEMPLATE_LOCAL_MACRO] =
{ 0L };

/** \brief  Example of declaring a local constant custom type. */
static const Template_GlobalStructureType Template_kt_LocalConstantCustomType =
{
   0U,
   0U,
   NULL_PTR,
   NULL_PTR };

/** \brief  Example of declaring a local constant custom type pointer. */
static const Template_GlobalStructureType * Template_kpt_LocalConstantPointerCustomType = NULL_PTR;

/** \brief  Example of declaring a local constant custom type array. */
static const Template_GlobalStructureType Template_kat_LocalConstantArrayCustomType[TEMPLATE_LOCAL_MACRO] =
{
   {
      0U,
      0U,
      NULL_PTR,
      NULL_PTR },
};

/** \brief  Example of declaring a local constant function pointer. */
static const Template_GlobalFunctionType * Template_kpf_LocalConstantPointerFunction = NULL_PTR;

/** \brief  Example of declaring a local constant array of function pointers. */
static const Template_GlobalFunctionType * Template_kapf_LocalConstantArrayPointerFunction[TEMPLATE_LOCAL_MACRO] =
{ NULL_PTR };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of declaring a global constant unsigned char. */
const uint8 Template_gkuc_GlobalConstantUnsignedChar;

/** \brief  Example of declaring a global constant unsigned char pointer. */
const uint8 * Template_gkpuc_GlobalConstantPointerUnsignedChar;

/** \brief  Example of declaring a global constant unsigned char array. */
const uint8 Template_gkauc_GlobalConstantArrayUnsignedChar[TEMPLATE_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant signed char. */
const sint8 Template_gksc_GlobalConstantSignedChar;

/** \brief  Example of declaring a global constant signed char pointer. */
const sint8 * Template_gkpsc_GlobalConstantPointerSignedChar;

/** \brief  Example of declaring a global constant signed char array. */
const sint8 Template_gkasc_GlobalConstantArraySignedChar[TEMPLATE_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant unsigned short. */
const uint16 Template_gkus_GlobalConstantUnsignedShort;

/** \brief  Example of declaring a global constant unsigned short pointer. */
const uint16 * Template_gkpus_GlobalConstantPointerUnsignedShort;

/** \brief  Example of declaring a global constant unsigned short array. */
const uint16 Template_gkaus_GlobalConstantArrayUnsignedShort[TEMPLATE_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant signed short. */
const sint16 Template_gkss_GlobalConstantSignedShort;

/** \brief  Example of declaring a global constant signed short pointer. */
const sint16 * Template_gkpss_GlobalConstantPointerSignedShort;

/** \brief  Example of declaring a global constant signed short array. */
const sint16 Template_gkass_GlobalConstantArraySignedShort[TEMPLATE_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant unsigned long. */
const uint32 Template_gkul_GlobalConstantUnsignedLong;

/** \brief  Example of declaring a global constant unsigned long pointer. */
const uint32 * Template_gkpul_GlobalConstantPointerUnsignedLong;

/** \brief  Example of declaring a global constant unsigned long array. */
const uint32 Template_gkaul_GlobalConstantArrayUnsignedLong[TEMPLATE_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant signed long. */
const sint32 Template_gksl_GlobalConstantSignedLong;

/** \brief  Example of declaring a global constant signed long pointer. */
const sint32 * Template_gkpsl_GlobalConstantPointerSignedLong;

/** \brief  Example of declaring a global constant signed long array. */
const sint32 Template_gkasl_GlobalConstantArraySignedLong[TEMPLATE_GLOBAL_MACRO_LENGTH];

/** \brief  Example of declaring a global constant custom type. */
const Template_GlobalStructureType Template_gkt_GlobalConstantCustomType;

/** \brief  Example of declaring a global constant custom type pointer. */
const Template_GlobalStructureType * Template_gkpt_GlobalConstantPointerCustomType;

/** \brief  Example of declaring a global constant custom type array. */
const Template_GlobalStructureType Template_gkat_GlobalConstantArrayCustomType[TEMPLATE_GLOBAL_MACRO_LENGTH];

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of declaring a local unsigned char variable. */
static uint8 Template_uc_LocalVariableUnsignedChar = 0U;

/** \brief  Example of declaring a local unsigned char pointer variable. */
static uint8 * Template_puc_LocalVariablePointerUnsignedChar = NULL_PTR;

/** \brief  Example of declaring a local unsigned char array. */
static uint8 Template_auc_LocalVariableArrayUnsignedChar[TEMPLATE_LOCAL_MACRO] =
{ 0U };

/** \brief  Example of declaring a local signed char variable. */
static sint8 Template_sc_LocalVariableSignedChar = 0;

/** \brief  Example of declaring a local signed char pointer variable. */
static sint8 * Template_psc_LocalVariablePointerSignedChar = NULL_PTR;

/** \brief  Example of declaring a local signed char array. */
static sint8 Template_asc_LocalVariableArraySignedChar[TEMPLATE_LOCAL_MACRO] =
{ 0 };

/** \brief  Example of declaring a local unsigned short variable. */
static uint16 Template_us_LocalVariableUnsignedShort = 0U;

/** \brief  Example of declaring a local unsigned short pointer variable. */
static uint16 * Template_pus_LocalVariablePointerUnsignedShort = NULL_PTR;

/** \brief  Example of declaring a local unsigned short array. */
static uint16 Template_aus_LocalVariableArrayUnsignedShort[TEMPLATE_LOCAL_MACRO] =
{ 0U };

/** \brief  Example of declaring a local signed short variable. */
static sint16 Template_ss_LocalVariableSignedShort = 0;

/** \brief  Example of declaring a local signed short pointer variable. */
static sint16 * Template_pss_LocalVariablePointerSignedShort = NULL_PTR;

/** \brief  Example of declaring a local signed short array. */
static sint16 Template_ass_LocalVariableArraySignedShort[TEMPLATE_LOCAL_MACRO] =
{ 0 };

/** \brief  Example of declaring a local unsigned long variable. */
static uint32 Template_ul_LocalVariableUnsignedLong = 0UL;

/** \brief  Example of declaring a local unsigned long pointer variable. */
static uint32 * Template_pul_LocalVariablePointerUnsignedLong = NULL_PTR;

/** \brief  Example of declaring a local unsigned long array. */
static uint32 Template_aul_LocalVariableArrayUnsignedLong[TEMPLATE_LOCAL_MACRO] =
{ 0UL };

/** \brief  Example of declaring a local signed long variable. */
static sint32 Template_sl_LocalVariableSignedLong = 0L;

/** \brief  Example of declaring a local signed long pointer variable. */
static sint32 * Template_psl_LocalVariablePointerSignedLong = NULL_PTR;

/** \brief  Example of declaring a local signed long array. */
static sint32 Template_asl_LocalVariableArraySignedLong[TEMPLATE_LOCAL_MACRO] =
{ 0L };

/** \brief  Example of declaring a local custom type variable. */
static Template_GlobalStructureType Template_t_LocalVariableCustomType =
{
   0U,
   0U,
   NULL_PTR,
   NULL_PTR };

/** \brief  Example of declaring a local custom type pointer variable. */
static Template_GlobalStructureType * Template_pt_LocalVariablePointerCustomType = NULL_PTR;

/** \brief  Example of declaring a local custom type array. */
static Template_GlobalStructureType Template_at_LocalVariableArrayCustomType[TEMPLATE_LOCAL_MACRO] =
{
   {
      0U,
      0U,
      NULL_PTR,
      NULL_PTR },
};

/** \brief  Example of declaring a local function pointer. */
static Template_GlobalFunctionType * Template_pf_LocalVariablePointerFunction = NULL_PTR;

/** \brief  Example of declaring a local array of function pointers. */
static Template_GlobalFunctionType * Template_apf_LocalVariableArrayPointerFunction[TEMPLATE_LOCAL_MACRO] =
{ NULL_PTR };

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/** \brief  Example of declaring a global unsigned char variable. */
uint8 Template_guc_GlobalVariableUnsignedChar = 0U;

/** \brief  Example of declaring a global unsigned char pointer. */
uint8 * Template_gpuc_GlobalVariablePointerUnsignedChar = NULL_PTR;

/** \brief  Example of declaring a global unsigned char array. */
uint8 Template_gauc_GlobalVariableArrayUnsignedChar[TEMPLATE_GLOBAL_MACRO_LENGTH] =
{ 0U };

/** \brief  Example of declaring a global signed char variable. */
sint8 Template_gsc_GlobalVariableSignedChar = 0;

/** \brief  Example of declaring a global signed char pointer. */
sint8 * Template_gpsc_GlobalVariablePointerSignedChar = NULL_PTR;

/** \brief  Example of declaring a global signed char array. */
sint8 Template_gasc_GlobalVariableArraySignedChar[TEMPLATE_GLOBAL_MACRO_LENGTH] =
{ 0 };

/** \brief  Example of declaring a global unsigned short variable. */
uint16 Template_gus_GlobalVariableUnsignedShort = 0U;

/** \brief  Example of declaring a global unsigned short pointer. */
uint16 * Template_gpus_GlobalVariablePointerUnsignedShort = NULL_PTR;

/** \brief  Example of declaring a global unsigned short array. */
uint16 Template_gaus_GlobalVariableArrayUnsignedShort[TEMPLATE_GLOBAL_MACRO_LENGTH] =
{ 0U };

/** \brief  Example of declaring a global signed short variable. */
sint16 Template_gss_GlobalVariableSignedShort = 0;

/** \brief  Example of declaring a global signed short pointer. */
sint16 * Template_gpss_GlobalVariablePointerSignedShort = NULL_PTR;

/** \brief  Example of declaring a global signed short array. */
sint16 Template_gass_GlobalVariableArraySignedShort[TEMPLATE_GLOBAL_MACRO_LENGTH] =
{ 0 };

/** \brief  Example of declaring a global unsigned long variable. */
uint32 Template_gul_GlobalVariableUnsignedLong = 0UL;

/** \brief  Example of declaring a global unsigned long pointer. */
uint32 * Template_gpul_GlobalVariablePointerUnsignedLong = NULL_PTR;

/** \brief  Example of declaring a global unsigned long array. */
uint32 Template_gaul_GlobalVariableArrayUnsignedLong[TEMPLATE_GLOBAL_MACRO_LENGTH] =
{ 0UL };

/** \brief  Example of declaring a global signed long variable. */
sint32 Template_gsl_GlobalVariableSignedLong = 0L;

/** \brief  Example of declaring a global signed long pointer. */
sint32 * Template_gpsl_GlobalVariablePointerSignedLong = NULL_PTR;

/** \brief  Example of declaring a global signed long array. */
sint32 Template_gasl_GlobalVariableArraySignedLong[TEMPLATE_GLOBAL_MACRO_LENGTH] =
{ 0L };

/** \brief  Example of declaring a global custom type variable. */
Template_GlobalStructureType Template_gt_GlobalVariableCustomType =
{
   0U,
   0U,
   NULL_PTR,
   NULL_PTR };

/** \brief  Example of declaring a global custom type pointer. */
Template_GlobalStructureType * Template_gpt_GlobalVariablePointerCustomType = NULL_PTR;

/** \brief  Example of declaring a global custom type array. */
Template_GlobalStructureType Template_gat_GlobalVariableArrayCustomType[TEMPLATE_GLOBAL_MACRO_LENGTH] =
{
   {
      0U,
      0U,
      NULL_PTR,
      NULL_PTR }
};

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

void Template_v_LocalFunctionNoReturn(uint8 uc_Param0, uint8 uc_Param1);
uint8 Template_uc_LocalFunctionUnsignedCharReturn(uint8 uc_Param);
uint8 * Template_puc_LocalFunctionPointerUnsignedCharReturn(void);
sint8 Template_sc_LocalFunctionSignedCharReturn(sint8 sc_Param);
sint8 * Template_psc_LocalFunctionPointerSignedCharReturn(void);
uint16 Template_us_LocalFunctionUnsignedShortReturn(uint16 us_Param);
uint16 * Template_pus_LocalFunctionPointerUnsignedShortReturn(void);
sint16 Template_ss_LocalFunctionSignedShortReturn(sint16 ss_Param);
sint16 * Template_pss_LocalFunctionPointerSignedShortReturn(void);
uint32 Template_ul_LocalFunctionUnsignedLongReturn(uint32 ul_Param);
uint32 * Template_pul_LocalFunctionPointerUnsignedLongReturn(void);
sint32 Template_sl_LocalFunctionSignedLongReturn(sint32 sl_Param);
sint32 * Template_psl_LocalFunctionPointerSignedLongReturn(void);
Template_GlobalStructureType Template_t_LocalFunctionCustomTypeReturn(Template_GlobalStructureType t_Param);
Template_GlobalStructureType * Template_pt_LocalFunctionPointerCustomTypeReturn(void);

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Example of local function.
 * \param      ucParam0 : First parameter description.
 * \param      ucParam1 : Second parameter description.
 * \return     -
 */
void Template_v_LocalFunctionNoReturn(uint8 uc_Param0, uint8 uc_Param1)
{
}

/**
 * \brief      Example of local function.
 * \param      ucParam : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
uint8 Template_uc_LocalFunctionUnsignedCharReturn(uint8 uc_Param)
{
   return (uint8) (uc_Param + Template_uc_LocalVariableUnsignedChar);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
uint8 * Template_puc_LocalFunctionPointerUnsignedCharReturn(void)
{
   return Template_puc_LocalVariablePointerUnsignedChar;
}

/**
 * \brief      Example of local function.
 * \param      scParam : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
sint8 Template_sc_LocalFunctionSignedCharReturn(sint8 sc_Param)
{
   return (sint8) (sc_Param + Template_sc_LocalVariableSignedChar);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
sint8 * Template_psc_LocalFunctionPointerSignedCharReturn(void)
{
   return Template_psc_LocalVariablePointerSignedChar;
}

/**
 * \brief      Example of local function.
 * \param      usParam : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
uint16 Template_us_LocalFunctionUnsignedShortReturn(uint16 us_Param)
{
   return (uint16) (us_Param + Template_us_LocalVariableUnsignedShort);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
uint16 * Template_pus_LocalFunctionPointerUnsignedShortReturn(void)
{
   return Template_pus_LocalVariablePointerUnsignedShort;
}

/**
 * \brief      Example of local function.
 * \param      ssParam : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
sint16 Template_ss_LocalFunctionSignedShortReturn(sint16 ss_Param)
{
   return (sint16) (ss_Param + Template_ss_LocalVariableSignedShort);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
sint16 * Template_pss_LocalFunctionPointerSignedShortReturn(void)
{
   return Template_pss_LocalVariablePointerSignedShort;
}

/**
 * \brief      Example of local function.
 * \param      ulParam : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
uint32 Template_ul_LocalFunctionUnsignedLongReturn(uint32 ul_Param)
{
   return (uint32) (ul_Param + Template_ul_LocalVariableUnsignedLong);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
uint32 * Template_pul_LocalFunctionPointerUnsignedLongReturn(void)
{
   return Template_pul_LocalVariablePointerUnsignedLong;
}

/**
 * \brief      Example of local function.
 * \param      slParam : Example parameter description.
 * \return     Sum between parameter and local variable.
 */
sint32 Template_sl_LocalFunctionSignedLongReturn(sint32 sl_Param)
{
   return (sint32) (sl_Param + Template_sl_LocalVariableSignedLong);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
sint32 * Template_psl_LocalFunctionPointerSignedLongReturn(void)
{
   return Template_psl_LocalVariablePointerSignedLong;
}

/**
 * \brief      Example of local function.
 * \param      slParam : Example parameter description.
 * \return     Static variable that does nothing since it's just an example.
 */
Template_GlobalStructureType Template_t_LocalFunctionCustomTypeReturn(Template_GlobalStructureType t_Param)
{
   Template_t_LocalVariableCustomType = t_Param;
   return (Template_t_LocalVariableCustomType);
}

/**
 * \brief      Example of local function.
 * \param      -
 * \return     Static variable that does nothing since it's just an example.
 */
Template_GlobalStructureType * Template_pt_LocalFunctionPointerCustomTypeReturn(void)
{
   return Template_pt_LocalVariablePointerCustomType;
}

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/

/**
 * \brief      Example of global function.
 * \param      -
 * \return     -
 */
void Template_gv_GlobalFunctionNoReturn(void)
{
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     -
 */
uint8 Template_guc_GlobalFunctionUnsignedCharReturn(void)
{
   return Template_guc_GlobalVariableUnsignedChar;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
uint8 * Template_gpuc_GlobalFunctionPointerUnsignedCharReturn(void)
{
   return Template_gpuc_GlobalVariablePointerUnsignedChar;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint8 Template_gsc_GlobalFunctionSignedCharReturn(void)
{
   return Template_gsc_GlobalVariableSignedChar;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint8 * Template_gpsc_GlobalFunctionPointerSignedCharReturn(void)
{
   return Template_gpsc_GlobalVariablePointerSignedChar;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
uint16 Template_gus_GlobalFunctionUnsignedShortReturn(void)
{
   return Template_gus_GlobalVariableUnsignedShort;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
uint16 * Template_gpus_GlobalFunctionPointerUnsignedShortReturn(void)
{
   return Template_gpus_GlobalVariablePointerUnsignedShort;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint16 Template_gss_GlobalFunctionSignedShortReturn(void)
{
   return Template_gss_GlobalVariableSignedShort;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint16 * Template_gpss_GlobalFunctionPointerSignedShortReturn(void)
{
   return Template_gpss_GlobalVariablePointerSignedShort;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
uint32 Template_gul_GlobalFunctionUnsignedLongReturn(void)
{
   return Template_gul_GlobalVariableUnsignedLong;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
uint32 * Template_gpul_GlobalFunctionPointerUnsignedLongReturn(void)
{
   return Template_gpul_GlobalVariablePointerUnsignedLong;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint32 Template_gsl_GlobalFunctionSignedLongReturn(void)
{
   return Template_gsl_GlobalVariableSignedLong;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
sint32 * Template_gpsl_GlobalFunctionPointerSignedLongReturn(void)
{
   return Template_gpsl_GlobalVariablePointerSignedLong;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
Template_GlobalStructureType Template_gt_GlobalFunctionCustomTypeReturn(void)
{
   return Template_gt_GlobalVariableCustomType;
}

/**
 * \brief      Example of global function.
 * \param      -
 * \return     Global variable that does nothing since it's just an example.
 */
Template_GlobalStructureType * Template_gpt_GlobalFunctionPointerCustomTypeReturn(void)
{
   return Template_gpt_GlobalVariablePointerCustomType;
}

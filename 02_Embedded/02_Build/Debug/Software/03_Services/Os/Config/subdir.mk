################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
D:/Work/Projects/Framework/02_Embedded/01_Software/03_Services/Os/Config/Os_Interrupts_Cfg.c \
D:/Work/Projects/Framework/02_Embedded/01_Software/03_Services/Os/Config/Os_Measurements_Cfg.c \
D:/Work/Projects/Framework/02_Embedded/01_Software/03_Services/Os/Config/Os_Tasks_Cfg.c \
D:/Work/Projects/Framework/02_Embedded/01_Software/03_Services/Os/Config/Os_Timer_Cfg.c 

OBJS += \
./Software/03_Services/Os/Config/Os_Interrupts_Cfg.o \
./Software/03_Services/Os/Config/Os_Measurements_Cfg.o \
./Software/03_Services/Os/Config/Os_Tasks_Cfg.o \
./Software/03_Services/Os/Config/Os_Timer_Cfg.o 

C_DEPS += \
./Software/03_Services/Os/Config/Os_Interrupts_Cfg.d \
./Software/03_Services/Os/Config/Os_Measurements_Cfg.d \
./Software/03_Services/Os/Config/Os_Tasks_Cfg.d \
./Software/03_Services/Os/Config/Os_Timer_Cfg.d 


# Each subdirectory must supply rules for building sources it contributes
Software/03_Services/Os/Config/Os_Interrupts_Cfg.o: D:/Work/Projects/Framework/02_Embedded/01_Software/03_Services/Os/Config/Os_Interrupts_Cfg.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=soft -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -I"../../01_Software/03_Services/EcuM/Generic" -I"../../01_Software/03_Services/Os/Config" -I"../../01_Software/03_Services/Os/Generic" -I"../../01_Software/05_MCAL/Mcu/Generic" -I"../../01_Software/07_Libraries/Device/Generic" -I"../../01_Software/07_Libraries/Linker/Generic" -I"../../01_Software/07_Libraries/RegInit/Generic" -I"../../01_Software/07_Libraries/Std/Generic" -I"../../01_Software/08_Startup/Generic" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Software/03_Services/Os/Config/Os_Measurements_Cfg.o: D:/Work/Projects/Framework/02_Embedded/01_Software/03_Services/Os/Config/Os_Measurements_Cfg.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=soft -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -I"../../01_Software/03_Services/EcuM/Generic" -I"../../01_Software/03_Services/Os/Config" -I"../../01_Software/03_Services/Os/Generic" -I"../../01_Software/05_MCAL/Mcu/Generic" -I"../../01_Software/07_Libraries/Device/Generic" -I"../../01_Software/07_Libraries/Linker/Generic" -I"../../01_Software/07_Libraries/RegInit/Generic" -I"../../01_Software/07_Libraries/Std/Generic" -I"../../01_Software/08_Startup/Generic" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Software/03_Services/Os/Config/Os_Tasks_Cfg.o: D:/Work/Projects/Framework/02_Embedded/01_Software/03_Services/Os/Config/Os_Tasks_Cfg.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=soft -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -I"../../01_Software/03_Services/EcuM/Generic" -I"../../01_Software/03_Services/Os/Config" -I"../../01_Software/03_Services/Os/Generic" -I"../../01_Software/05_MCAL/Mcu/Generic" -I"../../01_Software/07_Libraries/Device/Generic" -I"../../01_Software/07_Libraries/Linker/Generic" -I"../../01_Software/07_Libraries/RegInit/Generic" -I"../../01_Software/07_Libraries/Std/Generic" -I"../../01_Software/08_Startup/Generic" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Software/03_Services/Os/Config/Os_Timer_Cfg.o: D:/Work/Projects/Framework/02_Embedded/01_Software/03_Services/Os/Config/Os_Timer_Cfg.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=soft -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -I"../../01_Software/03_Services/EcuM/Generic" -I"../../01_Software/03_Services/Os/Config" -I"../../01_Software/03_Services/Os/Generic" -I"../../01_Software/05_MCAL/Mcu/Generic" -I"../../01_Software/07_Libraries/Device/Generic" -I"../../01_Software/07_Libraries/Linker/Generic" -I"../../01_Software/07_Libraries/RegInit/Generic" -I"../../01_Software/07_Libraries/Std/Generic" -I"../../01_Software/08_Startup/Generic" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


